from scripts.data import get_iris_data
import pandas as pd

df = get_iris_data()

def test_check_features():
    assert all(df.columns == ["SepalLengthCm", "SepalWidthCm", "PetalLengthCm", "PetalWidthCm", "Species"])

def test_check_nan():
    columns = df.columns.to_list()
    for column in columns:
        assert df[column].isnull().sum() == 0
