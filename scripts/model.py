import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
import mlflow
import mlflow.sklearn
from data import get_iris_data

df = pd.read_csv("../data/Iris.csv")
df = df.drop(["Id"], axis = 1)
# prepare the data (values)
X = df.drop(["Species"], axis=1)

# prepare the data (outcome)
y = df["Species"]

# define param
test_size = 0.3
random_state = 5
neighbors = 10

# splitting the data into training and test dataset
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=random_state)


# set mlflow experiment
mlflow.set_experiment(experiment_name="iris_test")

# log train_test_split param
mlflow.log_param("test_size", test_size)
mlflow.log_param("random_state", random_state)

# train a model (KNN)
knn = KNeighborsClassifier(n_neighbors=neighbors)
knn.fit(X_train, y_train)

# log training param
mlflow.log_param("n_neighbors", neighbors)


pred = knn.predict(X_test)

accuracy = accuracy_score(y_test, pred)
precision = precision_score(y_test, pred, average="micro")

#log metrics to mlflow
mlflow.log_metric("accuracy_score", accuracy)
mlflow.log_metric("precision_score", precision)


if accuracy > 0.96:
    mlflow.sklearn.log_model(knn, "model")