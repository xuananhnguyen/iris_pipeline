from train import train_knn
import train

knn = train_knn()
X_test = train.X_test

def predict_knn():
    pred = knn.predict(X_test)
    return pred