import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from data import get_iris_data
import mlflow
import mlflow.sklearn

df = get_iris_data()

# define param
test_size = 0.3
random_state = 5
neighbors = 5

# set mlflow experiment
mlflow.set_experiment(experiment_name = "iris_test")

#prepare the data (values)
X = df.drop(["Species"], axis = 1)
    
#prepare the data (outcome)
y = df["Species"]
    
# splitting the data into training and test dataset
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size = test_size, random_state = random_state)

# log train_test_split param
mlflow.log_param("test_size", test_size)
mlflow.log_param("random_state", random_state)
    
def train_knn():
    # train a model (KNN)
    knn = KNeighborsClassifier(n_neighbors = neighbors)
    knn.fit(X_train,y_train)

    # log training param
    mlflow.log_param("n_neighbors", neighbors)
    mlflow.sklearn.log_model(knn, "model")

    return knn
