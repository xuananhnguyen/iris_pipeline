from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from predict import predict_knn
import pandas as pd
import train

import mlflow

predict = predict_knn()

mlflow.set_experiment(experiment_name = "iris_test")

def messure_metrics(y_test = train.y_test, pred = predict):
    accuracy = accuracy_score(y_test, pred)
    precision = precision_score(y_test, pred, average="micro")

    #log metrics to mlflow
    mlflow.log_metric("accuracy_score", accuracy)
    mlflow.log_metric("precision_score", precision)
    
    metrics = {"Score" : [accuracy, precision],
              "Metrics" : ["Accuracy", "Precision"]}
    metrics_df = pd.DataFrame(metrics)
    metrics_df.index = metrics_df["Metrics"]
    del metrics_df["Metrics"]
    return metrics_df