import numpy as np 
from flask import Flask, request, jsonify, render_template
import pickle

app = Flask(__name__)
model = pickle.load(open("../models/model.pkl", "rb"))

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/predict", methods=["POST"])
def predict():
    print("Incoming request for prediction")
    features = [request.form.get("sepal_length"),request.form.get("sepal_width"),request.form.get("petal_length"),request.form.get("petal_width")]
    final_features = list(map(float, features))
    print(final_features)
    prediction = model.predict([np.array(final_features)])
    print("Responding with prediction "+"".join(prediction))
    return render_template("index.html", prediction_text ="The Species is "+"".join(prediction))

if __name__ == "__main__":
    app.run(port=8000)