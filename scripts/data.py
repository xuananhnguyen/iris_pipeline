import pandas as pd
import os

path = os.path.abspath("./data/Iris.csv")

def get_iris_data():
    """
    Load the iris data from filesystem

    :return:
    iris_df : pandas.DataFrame
        The iris flowers dataset
    """
    iris_df = pd.read_csv(path)
    iris_df = iris_df.drop(["Id"], axis = 1)
    return iris_df